<?php namespace Greymen\Maps;

//use Codestackers\Utils\Classes\Helpers;
use Greymen\Maps\Models\Treatment;
use October\Rain\Support\Facades\Event;
use RainLab\Pages\Classes\Page;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public $require = ['Fencus.GoogleMapsWidgets'];
    
    public function registerComponents()
    {
        return [
            Components\Locations::class         => 'locations',
            Components\LocationBySlug::class    => 'locationBySlug',
            Components\Mapbox::class            => 'Mapbox',
            Components\GoogleMaps::class        => 'GoogleMaps',
            Components\SearchStores::class      => 'SearchStores',
            Components\Breadcrumbs::class       => 'Breadcrumbs',
        ];
    }

    public function registerSettings()
    {
    }

    public function registerFormWidgets()
    {
        return [
            'Greymen\Maps\FormWidgets\AddressFinder' => [
                'label' => 'Address Finder',
                'code'  => 'addressfinder'
            ]
        ];
    }

    public function boot()
    {
        Page::extend(function($model) {
            $model->addDynamicMethod('getTreatmentItemOptions', function() {
                $treatments = [];
                foreach (Treatment::all() as $treatment) {
                    $treatments[$treatment->id] = $treatment->name;
                }
                return $treatments;
            });
        });

        // Strip page title
        // Event::listen('seo.beforeComponentRender', function ($component, $page) {
        //     $component->seoTag->meta_title = Helpers::stripInput($component->seoTag->meta_title);
        // });
    }
}
