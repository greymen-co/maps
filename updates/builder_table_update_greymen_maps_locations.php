<?php namespace Greymen\Maps\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateGreymenMapsLocations extends Migration
{
    public function up()
    {
        Schema::table('greymen_maps_locations', function($table)
        {
            $table->text('location')->nullable()->after('website');
        });
    }
    
    public function down()
    {
        Schema::table('greymen_maps_locations', function($table)
        {
            $table->dropColumn('location');
        });
    }
}
