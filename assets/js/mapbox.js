mapboxgl.accessToken = accessToken;

let popup = new mapboxgl.Popup({
    closeOnClick: false,
    offset: [0, -25]
});


let mapData;
var map = new mapboxgl.Map({
    container: 'map',
    style: mapStyle,
    center: [
        4.8929907, 52.3528627
    ],
    zoom: 6,
    logoPosition: 'top-right',
    attributionControl: false
});
let clickedMarkerId = null;


map.scrollZoom.disable();
var nav = new mapboxgl.NavigationControl();
map.addControl(nav, 'bottom-right');

map.on('load', function () {
    map.addSource('stores', {
        type: 'geojson',
        data: null,
        cluster: true,
        clusterMaxZoom: 14,
        clusterRadius: 50
    });

    map.loadImage(
        '/themes/default/assets/img/map-marker.png',
        function (err, image) {
            if (err)
                throw err;
            map.addImage('map-marker', image);
        }
    );

    map.loadImage(
        '/themes/default/assets/img/map-marker.png',
        function (err, image) {
            if (err)
                throw err;

            map.addImage('heart-cluster', image);
        }
    );

    map.addLayer({
        id: "storesLayer",
        type: "symbol",
        source: "stores",
        layout: {
            'icon-image': 'map-marker',
            'icon-size': 0.20,
        }
    });
    map.addLayer({
        id: "cluster-count",
        type: "symbol",
        source: "stores",
        filter: [
            "has", "point_count"
        ],
        layout: {
            'icon-image': 'map-marker',
            'icon-size': 0.35,
            "text-field": '{point_count_abbreviated}',
            "text-size": 12
        },
        paint: {
            "text-color": '#fff',
            "icon-color": '#f00'
        }
    });
    $("#map").show();
    map.resize();

    map.on('mouseenter', 'cluster-count', function() {
        map.getCanvas().style.cursor = 'pointer';
    });
    map.on('mouseleave', 'cluster-count', function() {
        map.getCanvas().style.cursor = '';
    });
    map.on('mouseenter', 'storesLayer', function() {
        map.getCanvas().style.cursor = 'pointer';
    });
    map.on('mouseleave', 'storesLayer', function() {
        map.getCanvas().style.cursor = '';
    });

    $.request('onGeoJSON', {
        success: function(data) {
            mapData=data;
            setGeoJSON(mapData);
        }
    })
});


map.on('click', function (e) {
    map
        .scrollZoom
        .enable();
    console.log(JSON.stringify(e.point) + ' | ' + JSON.stringify(e.lngLat) + ' | ' + map.getZoom());
})


map.on('click', 'storesLayer', function (e) {
    console.log('marker');
    let features = map.queryRenderedFeatures(e.point, { layers: ['storesLayer'] });
    clickedMarkerId= e.features[0].properties.id; //    .hasOwnProperty('id')  ? clickedMarker: null    
    var zoomLevel = 17;
    map.flyTo({
        center: e
            .features[0]
            .geometry
            .coordinates
        ,
        zoom: zoomLevel
    });
    if (features.length) {
        let feature = features[0];
        createPopUp(feature);
    }
});

map.on('click', 'cluster-count', function (e) {
    console.log('cluster');
    var zoomLevel = 10;
    if (map.getZoom() >= zoomLevel) zoomLevel = map.getZoom() + 1;
    map.flyTo({
        center: e
            .features[0]
            .geometry
            .coordinates,
        zoom: zoomLevel
    });
});


function createPopUp(feature) {
    var popupHtml = "<div class='map-popup'>" +feature.properties.popupHTML + "</div>";

    popup.remove();
    popup.setLngLat(feature.geometry.coordinates)
        .setHTML(popupHtml)
        .setLngLat(feature.geometry.coordinates)
        .addTo(map);
    $(".list-group").find("[data-id="+feature.properties.id+"]").addClass('active');
}

function centerMap() {
    stores = map.getSource('stores');
    console.log(stores);
    mapData = stores._data;
    if (! mapData) return false;
    var bounds = new mapboxgl.LngLatBounds();
    mapData.features.forEach(function (feature) {
        bounds.extend(feature.geometry.coordinates);
    });
    map.fitBounds(bounds, { maxZoom: 11, padding: 80 });
    return true;
}

function goStore(id) {
    feature = mapData.features[id];
    createPopUp(feature);
    map.flyTo({
        center: feature.geometry.coordinates,
        zoom: 16
    });
}


function returnToSearch() {
    $('#backToSearch').click(function () {
        $('.location-detail-item-1').css('z-index', 25);
        $('.location-detail-item-2').css('z-index', 5);
    });
}

function selectStore() {
    let storeLat = store.lat;
    let storeLng = store.lng;
    map.setCenter([storeLat, storeLng]);
}



function setGeoJSON(data) {
    mapData = (data);
    stores = map.getSource('stores');
    stores.setData(mapData);

    centerMap();
}

function getCurrentLocation() {
    if (navigator.geolocation) {
        $("#my-location").addClass('oc-loading');
        $("#my-location").html("fetching location");
        navigator.geolocation.getCurrentPosition(goCurrentLocation);
    } else {
        $("#my-location").html("Geolocation is not supported by this browser.");
    }
}

function goCurrentLocation(position) {
    $("#my-location").removeClass('oc-loading');
//    html = "lat: " + position.coords.latitude +" | lon: " + position.coords.longitude;
    html = "";
    map.flyTo({
        center: [ position.coords.longitude, position.coords.latitude],
        zoom: 14
    });
    $("#my-location").html(html);
}

function goCompanyLocation(obj) {
    map.flyTo({
        center: [ $(obj).data('lng'), $(obj).data('lat')],
        zoom: 14
    });
    clickedMarkerId= $(obj).data('id');
    createPopUp(mapData.features[$(obj).data('id') - 1]);
}

function setLocationInForm(data) {
    $('#search-stores-selection').addClass('list-group-item').show();
    $('#store_id').val(data.location.id);
    $('#store_name').val(data.location.name);

    $('.search-stores__input-container, #search-stores-results').hide();
    $('#s', '.search-stores__input-container').val('');
    $('#search-stores-results').empty();

    $('.search-stores__edit-location').click(removeLocationInForm);
}

function removeLocationInForm() {
    $('#store_id, #store_name').val('');
    $('#search-stores-selection').removeClass('list-group-item').hide().empty();
    $('.search-stores__input-container, #search-stores-results').show();
}

function searchResult(data)
{
    showSearch();
    feature = data["feature"] ?  JSON.parse(data["feature"]) : null;
    if (feature){
        map.flyTo({
            center: feature["center"],
            zoom: 12
        });
    } else {
//        centerMap();
    }
}

function showSearch(hide)
{
    return true;
}

function hideSearch(clean)
{
    showSearch(true);
    if(clean)
    {
        console.log(clean);
        $("input[name=s]").val("");
    }
    centerMap();
}

map.on('idle', function (e) {
    zoomLevel   = map.getZoom();
    center      = map.getCenter();
    console.log('Map is idle on zoomlevel: '+zoomLevel);

    if (zoomLevel > 7 )
    {
        if(zoomLevel < 8)
        {
            distance= 125;
        } else if(zoomLevel < 9) {
            distance= 50;
        } else if(zoomLevel < 10) {
            distance= 40;
        } else if(zoomLevel < 12) {
            distance= 15;
        } else {
            distance= 10;
        }
        $('#s').request('onSearch', {
            data : {
                lat:        center.lat,
                lng:        center.lng,
                distance:   distance,
                id:         clickedMarkerId
            }
        })
        if (! clickedMarkerId )  popup.remove();
        clickedMarkerId = null;
    } else {
        showSearch(true);
    }
})
popup.on('close', function(e) {
    if( ! popup.isOpen())
    {
        $(".list-group li").removeClass('active');
    }
})

