mapboxgl.accessToken = 'pk.eyJ1Ijoicm9iZXJ0bW0iLCJhIjoiY2s4aWt1eGtsMDNjYjNnbWptbW90NmM2aCJ9.R_5A0sBo-j7-6tkrAT2Q5Q';
apiKey = "AIzaSyC9a5od6WQXgU0yspy9BZeLAL0CqVNbFbE";

let mapData;
var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 6,
    center: {lat:4.8929907, lng: 52.3528627},
});


// map.scrollZoom.disable();
// map.addControl(new mapboxgl.NavigationControl());

// map.on('load', function () {
//     map.addSource('stores', {
//         type: 'geojson',
//         data: null,
//         cluster: true,
//         clusterMaxZoom: 14,
//         clusterRadius: 50
//     });

//     map.loadImage(
//         '/themes/default/assets/img/map-heart.png',
//         function (err, image) {
//             if (err)
//                 throw err;
//             map.addImage('heart-marker', image);
//         }
//     );

//     map.loadImage(
//         '/themes/default/assets/img/map-heart@2x.png',
//         function (err, image) {
//             if (err)
//                 throw err;

//             map.addImage('heart-cluster', image);
//         }
//     );
    
//     map.addLayer({
//         id: "storesLayer",
//         type: "symbol",
//         source: "stores",
//         layout: {
//             'icon-image': 'heart-marker',
//             'icon-size': 0.6
//         }
//     });
//     map.addLayer({
//         id: "cluster-count",
//         type: "symbol",
//         source: "stores",
//         filter: [
//             "has", "point_count"
//         ],
//         layout: {
//             'icon-image': 'heart-marker',
//             'icon-size': 1,
//             "text-field": '{point_count_abbreviated}',
//             "text-font": [
//                 "DIN Offc Pro Medium", "Arial Unicode MS Bold"
//             ],
//             "text-size": 12
//         },
//         paint: {
//             "text-color": '#fff',
//             "icon-color": '#f00'
//         }
//     });
//     $("#map").show();
//     map.resize();

//     $.request('onGeoJSON', {
//         success: function(data) {
//             mapData=data;
//             setGeoJSON(mapData);
//         }
//     })    
// });


// map.on('click', function (e) {
//     map
//         .scrollZoom
//         .enable();
// })

// map.on('click', 'storesLayer', function (e) {
//     console.log('marker');
//     let features = map.queryRenderedFeatures(e.point, { layers: ['storesLayer'] });
//     if (features.length) {
//         let feature = features[0];
//         createPopUp(feature);
//     }
//     var zoomLevel = 17;
//     map.flyTo({
//         center: e
//             .features[0]
//             .geometry
//             .coordinates
//         ,
//         zoom: zoomLevel
//     });

// });

// map.on('click', 'cluster-count', function (e) {
//     console.log('cluster');
//     var zoomLevel = 10;
//     if (map.getZoom() >= zoomLevel) zoomLevel = map.getZoom() + 1;
//     map.flyTo({
//         center: e
//             .features[0]
//             .geometry
//             .coordinates,
//         zoom: zoomLevel
//     });
// });


// function createPopUp(feature) {
//     popup.remove();
//     popup.setLngLat(feature.geometry.coordinates)
//         .setHTML('<div class="map-popup"><h4>' + feature.properties.name + '</h4>' + feature.properties.address + "<div>")
//         .setLngLat(feature.geometry.coordinates)
//         .addTo(map);
// }

// function centerMap() {
//     stores = map.getSource('stores');
//     console.log(stores);
//     mapData = stores._data;
//     if (! mapData) return false; 
//     var bounds = new mapboxgl.LngLatBounds();
//     mapData.features.forEach(function (feature) {
//         bounds.extend(feature.geometry.coordinates);
//     });
//     map.fitBounds(bounds, { maxZoom: 11, padding:40 });
// }

// function MapFlyTo(id) {
//     feature = mapData.features[id];
//     createPopUp(feature);
//     map.flyTo({
//         center: feature.geometry.coordinates,
//         zoom: 16
//     });
// }


// $("#country").change(function () {
//     lat = ($(this).find(':selected').data('lat'));
//     lon = ($(this).find(':selected').data('lon'));
//     zoom = ($(this).find(':selected').data('zoom'));
//     map.flyTo({
//         center: [lon, lat],
//         zoom: zoom
//     });
// });

// function returnToSearch() {
//     $('#backToSearch').click(function () {
//         $('.location-detail-item-1').css('z-index', 25);
//         $('.location-detail-item-2').css('z-index', 5);
//     });
// }

// function selectStore() {
//     let storeLat = store.lat;
//     let storeLng = store.lng;
//     map.setCenter([storeLat, storeLng]);
// }


// map.on('click', function (e) {
//     console.log(JSON.stringify(e.point) + ' | ' + JSON.stringify(e.lngLat) + ' | ' + map.getZoom());
// });

// function setGeoJSON(data) {
//     mapData = (data);
//     stores = map.getSource('stores');
//     stores.setData(mapData);

//     centerMap();
// }

// function getLocation() {
//     if (navigator.geolocation) {
//         $("#my-location").addClass('oc-loading');
//         $("#my-location").html("fetching location");
//         navigator.geolocation.getCurrentPosition(showPosition);
//     } else {
//         $("#my-location").html("Geolocation is not supported by this browser.");
//     }
// }

// function showPosition(position) {
//     $("#my-location").removeClass('oc-loading');
//     $("#my-location").html("");
//     html = "Latitude: " + position.coords.latitude +" | Longitude: " + position.coords.longitude;
//     $("#my-location").html(html);
// }

