<?php namespace Greymen\Maps\Components;

use Cms\Classes\ComponentBase;
use Greymen\Maps\Models\Location;

/**
 * Locations Component
 */


class Locations extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'locations Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'locationPage' => [
                'title'   => 'greymen.maps::lang.components.page',
                'type'    => 'string',
                'default' => "/praktijken/"
            ],

        ];
    }
    public function onRender()
    {
        $this->page['locationPage'] = $this->property('locationPage');
        $this->page['locations']    = Location::getLocations(3);
    }
}
