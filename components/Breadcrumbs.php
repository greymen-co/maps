<?php namespace Greymen\Maps\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Theme;
use RainLab\Pages\Classes\MenuItemReference;
use RainLab\Pages\Classes\Page as StaticPageClass;
use RainLab\Pages\Classes\Router;

/**
 * Class Breadcrumbs
 * @package Greymen\Maps\Components
 */
class Breadcrumbs extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Breadcrumbs Component',
            'description' => 'Generate breadcrumbs by static pages or URL.'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->page['breadcrumbs'] = $this->getItems();
    }

    /**
     * @return array
     * @throws \ApplicationException
     */
    public function getItems(): array
    {
        $items = $this->getItemsByStaticPages();
        if (empty($items)) {
            $items = $this->getItemsByUrl();
        }
        return $items;
    }

    /**
     * @return array
     */
    private function getItemsByUrl(): array
    {
        $items = [];
        $url = (!empty($this->page->breadcrumb_url)) ? $this->page->breadcrumb_url : $this->page->url;
        $url = substr($url, strpos($url, '/') + 1);
        if (empty($url)) {
            return $items;
        }
        $currentUrl = url('/');
        $slugs = explode('/', $url);
        $amountOfSlugs = count($slugs);
        foreach ($slugs as $key => $slug) {
            $currentUrl .= '/' . $slug;
            $item = new MenuItemReference();
            $item->url = $currentUrl;
            // Last item
            if ($amountOfSlugs === ($key + 1)) {
                $item->isActive = true;
                $item->title = (!empty($this->page->seo_title)) ? $this->page->seo_title : $this->page->title;
            } else {
                $item->isActive = false;
                $item->title = str_replace(['-', '_'], ' ', ucwords($slug));
            }
            $items[] = $item;
        }
        return $items;
    }

    /**
     * Get items by static pages
     *
     * @return array
     * @throws \ApplicationException
     */
    private function getItemsByStaticPages(): array
    {
        $url = $this->getRouter()->getUrl();
        if (!strlen($url)) {
            $url = '/';
        }
        $theme = Theme::getActiveTheme();
        $router = new Router($theme);
        $page = $router->findByUrl($url);
        if (!$page) {
            return [];
        }
        $tree = StaticPageClass::buildMenuTree($theme);
        $code = $startCode = $page->getBaseFileName();
        $breadcrumbs = [];
        while ($code) {
            if (!isset($tree[$code])) {
                break;
            }
            $pageInfo = $tree[$code];
            if ($pageInfo['navigation_hidden']) {
                $code = $pageInfo['parent'];
                continue;
            }
            $reference = new MenuItemReference();
            $reference->title = $pageInfo['title'];
            $reference->url = StaticPageClass::url($code);
            $reference->isActive = $code == $startCode;
            $breadcrumbs[] = $reference;
            $code = $pageInfo['parent'];
        }
        return array_reverse($breadcrumbs);
    }
}
