<?php namespace Greymen\Maps\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Locations extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\RelationController',
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';
    public $jsonable = ['data', 'opening_times', 'call_times'];

    public $requiredPermissions = [
        'admin-locations'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Greymen.Maps', 'main-menu-item', 'side-menu-locations');
    }
}
