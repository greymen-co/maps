<?php namespace Greymen\Maps\Models;

use Model;
use Greymen\Maps\Classes\MapboxCoder;
use Greymen\Maps\Models\Facility;

use DB;
use Illuminate\Support\Carbon;
use View;


/**
 * Model
 */
class Location extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    public $jsonable = ['data', 'opening_times', 'call_times', 'geodata', 'team_members', 'facilities', 'images'];

    public $belongsToMany = [
        'treatments' => [Treatment::class, 'table' => 'greymen_maps_location_treatment'],
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'greymen_maps_locations';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'postal_code' => 'max:6'
    ];
    

    public function getDayOfWeekAttribute()
    {
        $today = Carbon::now();
        return $today->locale('nl')->isoFormat('dddd');
    }

    
    public function getFacilitiesListAttribute()
    {
        $ret = [];
        if (empty($this->facilities)) {
            return $ret;
        }
        foreach ($this->facilities as $facility) {
            if (empty($facility['facilty_id'])) {
                continue;
            }
            $ret[] = Facility::find($facility['facilty_id']);
        }
        return $ret;
    }

    public static function getLocationBySlug($slug = '')
    {
        return SELF::where('slug', '=', $slug)->first();
    }

    public static function getLocations($limit = -1)
    {
        $items = new SELF;
        if ($limit > 0)
            $items = $items->limit($limit);
        return $items->get();
    }

    public function getDateTimes()
    {
        $today = Carbon::today();
        $allTimes = [];
        array_push($allTimes, $today->format("H:i"));
        for ($i = 0; $i <= 94; $i ++){
            $today->addMinutes(15);
            array_push($allTimes, $today->format("H:i"));
        }

        return $allTimes;
    }

    public static function getFaciltyIdOptions()
    {
        return Facility::getAll()->pluck('name', 'id');
    }

    /* SEARCH Functions for locator */
    public static function getGeoJSON()
    {
        $data = get() ? get() : post();

        $stores = SELF::where('active', 1)
            ->whereNotNull('lon');
        $ret["geodata"] = null;
        $ret["stores"] = $stores->get();
        $view  = View::make('greymen.maps::popup');
        $t = 0;
        foreach(@$ret['stores'] as $item )
        {
    
            $html = $view->with('location', $item)->render(); 
            $ret['stores'][$t]['popupHTML'] = addslashes(preg_replace( "/\r|\n|\t/", "", $html ));
            $t++;
        } 

        return $ret;

    }


    public static function getGeoByZipJSON()
    {
        $geoData = [];
        $error = null;
        $mapbox = new MapboxCoder;
        $translator = \RainLab\Translate\Classes\Translator::instance();
        $stores = SELF::where('active', 1)
            ->whereNotNull('lon')
            ->whereNotNull('address_1');

        $data = get();
        if (!isset($data)) $data = post();
        if (!isset($data['zip'])) $data['zip'] = '3605 KJ';
        if (!isset($data['country'])) $data['country'] = 'NL'; //$translator->getDefaultLocale();
        if (!isset($data['distance'])) $data['distance'] = 10; //$translator->getDefaultLocale();
        if ($data['distance'] > 10) $data['distance'] = 10;
        if ($data['distance'] < 1) $data['distance'] = 1;

        $stores->where('country_code', strtoupper($data['country']));
        if (isset($data['zip']) && strlen($data['zip']) > 2) {

            if (strtoupper($data['country']) == 'NL') {
                if (strlen($data['zip']) < 4) {
                    $error['zip'] = "too short";
                }
                if (strlen($data['zip']) > 4) $data['zip'] = substr($data['zip'], 0, 4);
            }
            $geoData = $mapbox->geoByZip($data['zip'], $data['country']);
            $geoData = isset($geoData['features'][0]) ? $geoData['features'][0] : null;
            if ($geoData) {
                $lat = $geoData['center'][1];
                $long = $geoData['center'][0];
                $stores =
                    Store::having('distance', '<=', $data['distance'])
                        ->select(DB::raw(
                            "*, (3959 * ACOS(COS(RADIANS($lat))
                        * COS(RADIANS(lat))
                        * COS(RADIANS($long) - RADIANS(lng))
                        + SIN(RADIANS($lat))
                        * SIN(RADIANS(lat)))) AS distance"
                        )
                        )->orderBy('distance', 'asc');
            } else {
                $stores->where('id', -1);
            }
        } else {
            $stores->where('zip', '1031HN')
                ->orderBy('name')->get();
        }
        $ret["error"] = $error;
        $ret["geodata"] = $geoData;
        $ret["stores"] = $stores->get()->toArray();
        return $ret;
    }

    public static function getLatLngByCity($city = "Amsterdam")
    {
        $mapbox = new MapboxCoder;
        $geoData = $mapbox->geoByCity($city);
        $feature = isset($geoData['features'][0]) ? $geoData['features'][0] : null;
        return $feature;
    }

    public static function getStoresByLatLng($lat = 52.370216000000, $long = 4.895168000000, $distance = 25)
    {
        if ($distance > 25) $distance = 125;
        return SELF::having('distance', '<', $distance)
            ->select(DB::raw(
                "*, (3959 * ACOS(COS(RADIANS($lat))
                    * COS(RADIANS(lat))
                    * COS(RADIANS($long) - RADIANS(lon))
                    + SIN(RADIANS($lat))
                    * SIN(RADIANS(lat)))) AS distance"
            )
            )
            ->where(function ($query) {
                $query->where('active', '=', 1)
                    ->orWhereNull('active');
            })
            ->orderBy('distance', 'asc')
            ->get();
    }

    public function scopeWithinBounds($query, $a, $b, $c, $d)
    {
        dd('scopeWithinBounds');
        return $query->where(function ($q) use ($a, $c) {
            $q->whereRaw("$a < $c")->whereBetween('lat', [$a, $c])
                ->orWhere(function ($q2) use ($a, $c) {
                    $q2->whereRaw("$a > $c")->whereBetween('lat', [$c, $a]);
                });
        })
            ->where(function ($q) use ($b, $d) {
                $q->whereRaw("$b < $d")->whereBetween('lon', [$b, $d])
                    ->orWhere(function ($q2) use ($b, $d) {
                        $q2->whereRaw("$b > $d")->whereBetween('lat', [$d, $b]);
                    });
            })
            ->get();
    }

    public function beforeSave()
    {
        $geodata = json_decode($this->geodata);
        $this->lat = isset($geodata->lat) ? $geodata->lat : null;
        $this->lon = isset($geodata->lat) ? $geodata->lng : null;

    }

    public function getOpeningDayOptions()
    {
        $options = [
            'maandag' => 'Maandag',
            'dinsdag' => 'Dinsdag',
            'woensdag' => 'Woensdag',
            'donderdag' => 'Donderdag',
            'vrijdag' => 'Vrijdag',
            'zaterdag' => 'Zaterdag',
            'zondag' => 'Zondag',
            'anders' => 'Anders',
        ];

        return $options;
    }


}
