<?php

namespace Greymen\Maps\Classes;

use Greymen\Maps\Models\Store;
use Backend\Models\ImportModel;

class StoreImport extends ImportModel
{
    public  $rules = [];
    public  $defaultOptions = [
        'firstRowTitles' => true,
        'delimiter' => ';',
        'enclosure' => null,
        'escape' => null,
        'encoding' => null
    ];
    public function test()
    {
        $data['address_1']      = "tweede atjehstraat 11"; 
        $data['zip']            = "1094KZ";
        $data['city']           = "Amsterdam";
        $data['country']        = "Netherlands";
        $mb = new MapboxCoder($data);
        dd($mb);
        return;
    }
    public function importData($results, $sessionKey = null)
    {
        foreach ($results as $row => $data) {
            try {
                $data['id']= intval($data['id']);
                $data['active']= 1;

                $store = Store::findOrNew($data['id']);
                $data['website'] = isset($data['website']) ? $data['website'] : '';
                if (! $store->id) {
                    //Create
                    $gc = new MapboxCoder($data);
                    $store->fill($data);
//                    $store->website = str_start($data['website'], 'http://');
                    $store->lat = $gc->lat;
                    $store->lng = $gc->lng;
                    $store->active  = 1;
                    $store->geodata = json_encode($gc->resp);
                    $store->address = $gc->formattedAddress;
                    $store->id = $data['id'];
                    $store->products = $data['products'];
                    $store->save();
                    $this->logCreated();
                } else {
                    //Update
                    if ($data['address_1'] != $store->address_1 
                        || is_null($store->lat)
                        || $data['city'] != $store->city
                        || $data['country'] != $store->country
                    )
                    {
                        $gc = new MapboxCoder($data);

                        $store->lat = $gc->lat;
                        $store->lng = $gc->lng;
                        $store->address = $gc->formattedAddress;

                        $store->geodata = json_encode($gc->resp);
                        $store->fill($data);
                        $store->website = str_start($data['website'], 'http://');
                        $store->save();
                        $this->logUpdated();
                    } else {

                        $store->active  = 1;
                        $store->fill($data);
                        $store->save();
                        $this->logUpdated();
                    }
                }
            }
            catch (\Exception $ex) {
                $this->logError($row, $ex->getMessage());
            }
        }
    }
}

