<?php namespace Greymen\Maps\Components;

use Cms\Classes\ComponentBase;
use Greymen\Maps\Models\Store;
use Greymen\Maps\Models\Countries;
use System\Classes\CombineAssets;
use Response;

class GoogleMaps extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Store Locator GoogleMaps Component',
            'description' => 'GoogleMaps with marked stores'
        ];
    }

    public function defineProperties()
    {
        return [
            'key' => [
                'title'             => 'Mapbox key',
                'default'           => null,
                'type'              => 'string',
           ]            
        ];
    }

    public function onGeoJSON()
    {
        $data = Store::getGeoJSON();
        $json = json_decode($this->renderPartial('Mapbox::geojson.htm', ['data' => $data]));
        return Response::json($json);
    }

    public function onTrue()
    {
        return true;
    }

    public function onRun()
    {
        $this->addCss('/plugins/greymen/maps/assets/css/store-locator.css');
        $this->addJs('/plugins/greymen/maps/assets/js/googlemaps.js');
    }

    public function onOnlineStores() 
    {

        return [
            '.online-stores'   => $this->renderPartial('Mapbox::onlinestores.htm', ['showHeader' => true]),
        ];
    }

    public function onStoreLocator() 
    {
        $data['countries'] = Countries::get();
        $data['showHeader'] = true;
        return [
            '#map'   => $this->renderPartial('Mapbox::mapbox.htm',  $data),
        ];
    }

    public function onMapResults() {
        $translator         = \RainLab\Translate\Classes\Translator::instance();
        $activeLocale       = $translator->getLocale(); 
        $stores             = Store::getGeoJSON();
//dd($stores);
        $data['stores']     = $stores['stores'];
        $data['error']      = $stores['error'];
        $data['geodata']    = json_encode($stores['geodata']);
        $data['zipcode']    = post('zip');
        $data['distance']   = post('distance');
        $data['country']    = post('country') ? post('country'): 'nl';
        return [
            '.results-inner' => $this->renderPartial('Mapbox::showresults.htm',$data)
        ];
    }

}
