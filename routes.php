<?php

namespace Greymen\Maps;

use Greymen\Maps\Models\Location;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;

Route::get('/api/stores.json', function () {
    return Location::distance(get('lat'), get('lon'), get('radius'));
//    return Location::withinBounds( get('a'), get('b'), get('c'), get('d') );

//    return Location::withinBounds( get('a'), get('b'), get('c'), get('d') );
    // return get('lat')
    //     ? Location::distance(get('lat'), get('lon'), get('radius'))
    //     : Location::withinBounds( get('a'), get('b'), get('c'), get('d') );
});

Route::get('/api/geo.json', 'Greymen\Maps\Classes\MapboxCoder@getGeoJSON');
//Route::get('/api/geo.json', 'Greymen\Maps\Components\Mapbox@onApiGeoJSON');


Route::get('/import/map/test', 'Greymen\Maps\Classes\StoreImport@test');

Route::get('/mapbox', function () {
    $html = file_get_contents('plugins/greymen/maps/components/mapbox/mapbox.htm');
    $apiKey = Config::get('greymen.maps::mapboxKey');
    return str_replace('%api_key%', $apiKey, $html) ;
});
