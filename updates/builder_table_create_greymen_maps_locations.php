<?php namespace Greymen\Maps\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGreymenMapsLocations extends Migration
{
    public function up()
    {
        Schema::create('greymen_maps_locations', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 100)->nullable();
            $table->string('slug', 100)->nullable();
            $table->string('street_name', 100)->nullable();
            $table->string('postal_code', 6)->nullable();
            $table->longText('data')->nullable();
            $table->text('call_times')->nullable();
            $table->text('opening_times')->nullable();
            $table->decimal('lat', 16, 12)->nullable();
            $table->decimal('lon', 16, 12)->nullable();
            $table->text('geodata')->nullable();
            $table->boolean('active')->nullable();
            $table->integer('sort_order')->default(0);
            $table->text('images')->nullable();
            $table->string('contact_person', 100)->nullable();
            $table->string('city', 100)->nullable();
            $table->string('website', 200)->nullable();
            $table->string('google_maps_url', 512)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->unique('slug');
            $table->index('name');
            $table->index('active');
            $table->index('sort_order');
            $table->index('created_at');
        });        
    }
    
    
    public function down()
    {
        Schema::dropIfExists('greymen_maps_locations');
    }
}