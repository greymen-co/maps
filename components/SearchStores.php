<?php namespace Greymen\Maps\Components;

use Cms\Classes\ComponentBase;
use Greymen\Maps\Models\Location;

class SearchStores extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Search Location Component',
            'description' => 'Search MZGN locations'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onSearch()
    {
        $feature                = null;
        $data['keyword']        = post('s');
        $data['lat']            = post('lat');
        $data['lng']            = post('lng');
        $data['distance']       = post('distance');
        $data['id']             = post('id');
        $data['showHeader']     = true;

        if (post('lat') & post('lng'))
        {
            $data['stores'] = Location::getStoresByLatLng($data['lat'],$data['lng'], $data['distance']);
        } else  {
            $feature   = Location::getLatLngByCity($data['keyword']);
            if ($feature ) {
                $data['stores'] = Location::getStoresByLatLng($feature['center'][1], $feature['center'][0] );
            }
        }
        return [
            '#search-stores-results'    => $this->renderPartial('SearchStores::result.htm',  $data),
            'feature'                  => $feature ? json_encode($feature) : null,
        ];
    }


    public function onDropdownSearch()
    {
        $feature                = null;
        $data['keyword']        = post('s');
        $data['lat']            = post('lat');
        $data['lng']            = post('lng');
        $data['distance']       = post('distance');
        $data['showHeader']     = true;

        if (post('lat') & post('lng'))
        {
            $data['stores'] = Location::getStoresByLatLng($data['lat'],$data['lng'], $data['distance']);
        } else  {
            $feature   = Location::getLatLngByCity($data['keyword']);
            if ($feature ) {
                $data['stores'] = Location::getStoresByLatLng($feature['center'][1], $feature['center'][0] );
            }
        }
        return [
            '#search-stores-results'    => $this->renderPartial('SearchStores::dropdownresult.htm',  $data),
            'feature'                  => $feature ? json_encode($feature) : null,
        ];
    }

    public function onDropdownSelect()
    {
        $location_id = intval(post('location_id'));
        $location = Location::find($location_id);
        if (!$location) {
            return [];
        }

        return [
            '#search-stores-selection' => $this->renderPartial('SearchStores::dropdownselection.htm', [
                'location' => $location,
            ]),
            '.search-stores-details'    => $this->renderPartial('SearchStores::details.htm', [
                'location' => $location,
            ]),

            'location' => [
                'id' => $location->id,
                'name' => $location->name,
            ],
        ];
    }

}
