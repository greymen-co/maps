<?php namespace Greymen\Maps\Components;

use Cms\Classes\ComponentBase;
use Greymen\Maps\Models\Location;

/**
 * LocationBySlug Component
 */
class LocationBySlug extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'LocationBySlug Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'   => 'greymen.maps::lang.components.slug',
                'type'    => 'string',
                'default' => '{{ :slug }}'
            ],
            'campaign' => [
                'title'   => 'greymen.maps::lang.components.campaign',
                'type'    => 'string',
                'default' => '{{ :campaign }}'
            ],
        ];
    }

    public function onRun()
    {
        $this->page['location'] = Location::getLocationBySlug($this->param('slug'));
        $this->page['is_campaign'] = (!empty($this->param('campaign')));
        $this->addComponent(TreatmentItems::class, 'treatmentItemsPartial', []);

        if (!is_a($this->page['location'], Location::class)) {
            return \Redirect::to('404');
        }
        // dd($this->page['location']);

        $this->page->title = $this->page['location']->name;
        $this->page->seo_title = $this->page->title;
//        $this->page->seo_description = '';

        if ($this->page['is_campaign']) {
            $this->page['body_class'] = 'is-campaign';
        }
    }
}
